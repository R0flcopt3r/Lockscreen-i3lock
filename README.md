# Lockscreen-i3lock

This bash script makes i3lock a little more fancy. It's made to work with
multiple monitors automatically with zero configuring required. It is not
guaranteed to work, nor will I hold myself responsible if it breaks your system
in any way.

It grabs monitor sizes and offsets from XRandR and takes a screenshot with scrot
then blurs it and places `lock.png` with ImageMagick. It's rather slow if
you have a lot of pixels and/or a weak processor.

I've also added so DPMS is enabled when the lockscreen is active, the screen
will shut off within 10 seconds, when it's unlocked, it will disable DPMS.

To use it simply run the script, `./i3lock-multihead`.

If you wish to permanently install it run `sudo make install`.

## Extras

I've added two example systemd servies, one to enable xautolock
`systemd/xautolock.service` and one for autolocking when the machine is put to
sleep `systemd/sleeplock.service`. You'll need to change the username in each
files before using them, and also install `xautolock`. `xautolock.service` locks
the screen after 5 minutes of inactivity.

## Dependencies
* i3locks
* ImageMagick
* scrot
* xrandr

## Examples

Below are some example pictures of various monitor setups and benchmarks. This
was run on a i7 7700hq with fedora 30.

**One monitor** *1920x1080* `./i3lock-multihead  0.64s user 0.07s system 132% cpu 0.534 total`
![screenshot](./example1.png)

**Two monitors** *4480x1440* `./i3lock-multihead  1.46s user 0.15s system 140% cpu 1.147 total`
![screenshot](./example2.png)

**Three monitors** *6160x1800* `./i3lock-multihead  2.25s user 0.22s system 127% cpu 1.934 total`
![screenshot](./example3.png)

