
PRGM = i3lock-multihead
SUFFIX ?= src
PREFIX ?= /usr
SHRDIR ?= $(PREFIX)/share
BINDIR ?= $(PREFIX)/bin

install:
	@install -Dm755 $(SUFFIX)/$(PRGM)  	-t $(DESTDIR)$(BINDIR)
	@install -Dm644 $(SUFFIX)/lock.png	-t $(DESTDIR)$(SHRDIR)/$(PRGM)/icons
	@install -Dm644 LICENSE             -t $(DESTDIR)$(SHRDIR)/licenses/$(PRGM)
